#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sqfs/compressor.h>
#include <sqfs/dir_reader.h>
#include <sqfs/id_table.h>
#include <sqfs/inode.h>
#include <sqfs/super.h>
#include <sqfs/io.h>
#include <sqfs/frag_table.h>
#include <sqfs/block.h>

#include <openssl/sha.h>

#include <sqlite3.h>

#include <cwalk.h>

#define HASH_SIZE SHA256_DIGEST_LENGTH
#define HASH_STRING_SIZE (2*HASH_SIZE+1)

enum {
    DELTAGEN_SUCCESS,
    DELTAGEN_FAILED_TO_OPEN_SQUASHFS,
    DELTAGEN_FAILED_TO_READ_SUPER_BLOCK,
    DELTAGEN_INVALID_COMPRESSOR_CONFIG,
    DELTAGEN_ERROR_CREATING_COMPRESSOR,
    DELTAGEN_ERROR_CREATING_ID_TABLE,
    DELTAGEN_FAILED_TO_READ_ID_TABLE,
    DELTAGEN_ERROR_CREATING_FRAGMENT_TABLE,
    DELTAGEN_FAILED_TO_READ_FRAGMENT_TABLE,
    DELTAGEN_ERROR_CREATING_DIRECTORY_READER,
    DELTAGEN_FAILED_TO_GET_DIRECTORY_HIERARCHY,
    DELTAGEN_FAILED_TO_OPEN_DB,
    DELTAGEN_FAILED_TO_INITIALIZE_DB,
    DELTAGEN_FAILED_TO_CREATE_IMAGE_RECORD,
    DELTAGEN_FAILED_TO_GET_IMAGE_NAME,
    DATAGEN_FAILED_TO_BIND_SQL_PARAMETER,
    DELTAGEN_FAILED_TO_WRITE_BLOCK,
};

static char *to_owned(const char *str) {
    if (str == NULL) {
        return NULL;
    }

    size_t str_length = strlen(str);
    char *owned_str = calloc(str_length + 1, sizeof(char));
    memcpy(owned_str, str, str_length);
    return owned_str;
}

typedef struct freeable {
    void (*free_func)(void *data);
} freeable_t;

void deltagen_free(void *data) {
    if (data == NULL) {
        return;
    }

    freeable_t *freeable = data;

    freeable->free_func(data);
}

typedef struct image {
    freeable_t freeable;
    uint32_t id;
    char *name;
} image_t;

typedef struct block_to_insert {
    uint8_t hash[HASH_SIZE];
    uint64_t start_offset;
    uint32_t size;
} block_to_insert_t;

typedef struct context {
    freeable_t freeable;
    sqlite3 *db_handle;
    image_t *image;
    struct {
        sqfs_file_t *file;
        sqfs_compressor_t *compressor;
        sqfs_id_table_t *id_table;
        sqfs_frag_table_t *fragment_table;
        sqfs_dir_reader_t *directory_reader;
        sqfs_tree_node_t *root_node;
        sqfs_super_t super_block;
        sqfs_compressor_config_t compressor_config;
    } sqfs;
} context_t;

void image_free(void *data) {
    image_t *image = data;

    free(image->name);
    free(image);
}

image_t *image_create() {
    image_t *image = calloc(1, sizeof(image_t));
    image->freeable.free_func = image_free;

    return image;
}

size_t context_write_block_to_insert(context_t *context, const block_to_insert_t *block_to_insert) {
    const char *sql = "INSERT INTO blocks (hash, start_offset, size, image_id) VALUES (?, ?, ?, ?)";
    size_t status = DELTAGEN_SUCCESS;

    sqlite3_stmt *stmt = NULL;
    sqlite3_prepare_v3(context->db_handle, sql, -1, 0, &stmt, NULL);

    if (sqlite3_bind_blob(stmt, 1, &block_to_insert->hash, HASH_SIZE, SQLITE_STATIC) ||
        sqlite3_bind_int64(stmt, 2, (int64_t) block_to_insert->start_offset) ||
        sqlite3_bind_int(stmt, 3, (int32_t) block_to_insert->size) ||
        sqlite3_bind_int(stmt, 4, (int32_t) context->image->id)) {
        status = DATAGEN_FAILED_TO_BIND_SQL_PARAMETER;
        goto out;
    }

    size_t ret = sqlite3_step(stmt);

    if (ret != SQLITE_DONE) {
        status = DELTAGEN_FAILED_TO_WRITE_BLOCK;
    }

    out:
    sqlite3_finalize(stmt);
    return status;
}

void context_free(void *data) {
    context_t *context = data;

    sqlite3_close_v2(context->db_handle);

    deltagen_free(context->image);

    sqfs_destroy(context->sqfs.root_node);
    sqfs_destroy(context->sqfs.directory_reader);
    sqfs_destroy(context->sqfs.fragment_table);
    sqfs_destroy(context->sqfs.id_table);
    sqfs_destroy(context->sqfs.compressor);
    sqfs_destroy(context->sqfs.file);

    free(context);
}

context_t *context_create() {
    context_t *context = calloc(1, sizeof(context_t));
    context->freeable.free_func = context_free;

    return context;
}

static ssize_t initialize_db(sqlite3 *db_handle) {
    const char *const sql =
            "BEGIN;\n"
            "PRAGMA foreign_keys = ON;\n"
            "CREATE TABLE IF NOT EXISTS images (\n"
            "  id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL,\n"
            "  name TEXT NOT NULL UNIQUE\n"
            ");\n"
            "CREATE TABLE IF NOT EXISTS blocks (\n"
            "  hash BLOB(32) PRIMARY KEY NOT NULL UNIQUE,\n"
            "  start_offset INTEGER(8) NOT NULL,\n"
            "  size INTEGER(4) NOT NULL,\n"
            "  image_id INTEGER NOT NULL,\n"
            "  FOREIGN KEY (image_id) REFERENCES images(id) ON DELETE CASCADE\n"
            ");\n"
            "COMMIT;";

    return sqlite3_exec(db_handle, sql, NULL, NULL, NULL);
}

uint32_t image_id_from_stmt(sqlite3_stmt *stmt) {
    return sqlite3_column_int(stmt, 0);
}

char *image_name_from_stmt(sqlite3_stmt *stmt) {
    return to_owned((char *) sqlite3_column_text(stmt, 1));
}

static void populate_p_image(image_t **p_image, uint32_t id, char *name) {
    if (p_image == NULL) {
        return;
    }
    image_t *image = image_create();
    image->id = id;
    image->name = name;
    *p_image = image;

}

static ssize_t try_create_image_record(sqlite3 *db_handle, const char *image_name, image_t **p_image) {
    ssize_t status = DELTAGEN_SUCCESS;
    const char *const sql =
            "INSERT INTO images (name) VALUES (?) RETURNING id, name;";
    sqlite3_stmt *stmt = NULL;


    if ((status = sqlite3_prepare_v3(db_handle, sql, -1, 0, &stmt, NULL))) {
        goto out;
    }

    if ((status = sqlite3_bind_text(stmt, 1, image_name, -1, SQLITE_STATIC))) {
        goto out;
    }

    if ((status = sqlite3_step(stmt)) == SQLITE_ROW) {
        populate_p_image(p_image, image_id_from_stmt(stmt), image_name_from_stmt(stmt));
    }

    if ((status = sqlite3_step(stmt)) == SQLITE_DONE) {
        status = 0;
    }

    out:
    sqlite3_finalize(stmt);
    return status;
}

size_t context_init(context_t *context, const char *const sqfs_path, const char *const db_path) {
    context->sqfs.file = sqfs_open_file(sqfs_path, SQFS_FILE_OPEN_READ_ONLY);
    if (context->sqfs.file == NULL) {
        return DELTAGEN_FAILED_TO_OPEN_SQUASHFS;
    }

    if (sqlite3_open_v2(db_path, &context->db_handle, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL)) {
        return DELTAGEN_FAILED_TO_OPEN_DB;
    }

    if (initialize_db(context->db_handle)) {
        return DELTAGEN_FAILED_TO_INITIALIZE_DB;
    }

    const char *img_name = NULL;
    cwk_path_get_basename(sqfs_path, &img_name, NULL);

    if (img_name == NULL) {
        return DELTAGEN_FAILED_TO_GET_IMAGE_NAME;
    }

    if (try_create_image_record(context->db_handle, img_name, &context->image)) {
        return DELTAGEN_FAILED_TO_CREATE_IMAGE_RECORD;
    }

    if (sqfs_super_read(&context->sqfs.super_block, context->sqfs.file)) {
        return DELTAGEN_FAILED_TO_READ_SUPER_BLOCK;
    }

    if (sqfs_compressor_config_init(&context->sqfs.compressor_config, context->sqfs.super_block.compression_id,
                                    context->sqfs.super_block.block_size, SQFS_COMP_FLAG_UNCOMPRESS)) {
        return DELTAGEN_INVALID_COMPRESSOR_CONFIG;
    }

    if (sqfs_compressor_create(&context->sqfs.compressor_config, &context->sqfs.compressor)) {
        return DELTAGEN_ERROR_CREATING_COMPRESSOR;
    }

    if ((context->sqfs.id_table = sqfs_id_table_create(0)) == NULL) {
        return DELTAGEN_ERROR_CREATING_ID_TABLE;
    }

        if (sqfs_id_table_read(context->sqfs.id_table, context->sqfs.file, &context->sqfs.super_block,
                           context->sqfs.compressor)) {
        return DELTAGEN_FAILED_TO_READ_ID_TABLE;
    }

    if ((context->sqfs.fragment_table = sqfs_frag_table_create(0)) == NULL) {
        return DELTAGEN_ERROR_CREATING_FRAGMENT_TABLE;
    }

    if (sqfs_frag_table_read(context->sqfs.fragment_table, context->sqfs.file, &context->sqfs.super_block,
                             context->sqfs.compressor)) {
        return DELTAGEN_FAILED_TO_READ_FRAGMENT_TABLE;
    }

    if ((context->sqfs.directory_reader = sqfs_dir_reader_create(&context->sqfs.super_block, context->sqfs.compressor,
                                                                 context->sqfs.file, 0)) == NULL) {
        return DELTAGEN_ERROR_CREATING_DIRECTORY_READER;
    }

    if (sqfs_dir_reader_get_full_hierarchy(context->sqfs.directory_reader, context->sqfs.id_table, NULL, 0,
                                           &context->sqfs.root_node)) {
        return DELTAGEN_FAILED_TO_GET_DIRECTORY_HIERARCHY;
    }

    return DELTAGEN_SUCCESS;
}

void bytesToHexString(const uint8_t *const in, size_t size, char *const out) {
    for (size_t i = 0; i < size; i++) {
        sprintf(&out[i * 2], "%02X", in[i]);
    }
}

void block_to_insert_generate_hash(block_to_insert_t *block_to_insert, const sqfs_u8 *block_data) {
    SHA256(block_data, block_to_insert->size, block_to_insert->hash);
}

void print_hash(const sqfs_tree_node_t *node, const size_t index, const uint8_t hash[]) {
    char str_hash[HASH_STRING_SIZE] = {0};
    bytesToHexString(hash, SHA256_DIGEST_LENGTH, str_hash);
    printf("%s(%zu): %s\n", node->name, index, str_hash);
}

static ssize_t
get_block_data(const context_t *context, uint64_t start_offset, uint32_t size, uint8_t *block_data) {
    ssize_t status = context->sqfs.file->read_at(context->sqfs.file, start_offset, block_data,
                                                 size);
    return status;
}

static ssize_t hash_dir_tree(context_t *context, const sqfs_tree_node_t *node) {
    ssize_t status = 0;

    if (node == NULL) {
        return status;
    }

    sqfs_inode_generic_t *inode = node->inode;

    if (inode->base.type == SQFS_INODE_FILE || inode->base.type == SQFS_INODE_EXT_FILE) {
        uint64_t file_size = 0;
        block_to_insert_t block_to_insert = {0};
        sqfs_inode_get_file_size(inode, &file_size);
        sqfs_inode_get_file_block_start(inode, &block_to_insert.start_offset);
        size_t count = sqfs_inode_get_file_block_count(inode);
        uint8_t *block_data = calloc(context->sqfs.super_block.block_size, sizeof(uint8_t));

        for (size_t index = 0; index < count; ++index) {
            block_to_insert.start_offset += block_to_insert.size;
            block_to_insert.size = SQFS_ON_DISK_BLOCK_SIZE(inode->extra[index]);

            get_block_data(context, block_to_insert.start_offset, block_to_insert.size, block_data);

            block_to_insert_generate_hash(&block_to_insert, block_data);
            context_write_block_to_insert(context, &block_to_insert);

        }

        free(block_data);
    }

    for (node = node->children; node != NULL; node = node->next) {
        if ((status = hash_dir_tree(context, node))) {
            return status;
        }
    }

    return status;
}


static ssize_t
walk_fragment_table(context_t *context) {
    block_to_insert_t block_to_insert = {0};
    sqfs_fragment_t fragment = {0};
    ssize_t status = 0;
    size_t entries = sqfs_frag_table_get_size(context->sqfs.fragment_table);
    uint8_t *const block_data = calloc(context->sqfs.super_block.block_size, sizeof(uint8_t));

    for (uint32_t index = 0; index < entries; ++index) {
        if ((status = sqfs_frag_table_lookup(context->sqfs.fragment_table, index, &fragment))) {
            goto out;
        }

        block_to_insert.start_offset = fragment.start_offset;
        block_to_insert.size = SQFS_ON_DISK_BLOCK_SIZE(fragment.size);
        if ((status = get_block_data(context, block_to_insert.start_offset, block_to_insert.size, block_data))) {
            goto out;
        }

        block_to_insert_generate_hash(&block_to_insert, block_data);
        context_write_block_to_insert(context, &block_to_insert);
    }

    out:
    free(block_data);
    return status;
}

int main(int argc, char **argv) {
    int status = EXIT_FAILURE;
    context_t *context = context_create();

    /* open the SquashFS file we want to read */
    if (argc != 2) {
        fputs("Usage: list_filadd indexes <squashfs-file>\n", stderr);
        return EXIT_FAILURE;
    }

    if (context_init(context, argv[1], "db.sqlite")) {
        goto out;
    }

    hash_dir_tree(context, context->sqfs.root_node);
    walk_fragment_table(context);

    /* cleanup */
    status = EXIT_SUCCESS;
    out:
    deltagen_free(context);
    return status;
}
